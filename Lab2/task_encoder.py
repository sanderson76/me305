# -*- coding: utf-8 -*-
"""
Created on Thu Jan 27 08:49:06 2022

@author: Scott Anderson
@author: Spencer Alonge
"""
from time import ticks_us, ticks_add, ticks_diff
import shares
import encoder
import pyb

def taskEncFcn(taskName, period, zflag, pshare, dshare ):
    '''!@brief          A method that determines the state of the system
                        and is responsible for updating encoder object
                        at requisite interval.
        @details        Updates the position each interval and calls encoder
                        methods if state is changed.
        @param taskName The name of the task.
        @param period   The amount of time between each update (microseconds).
        @param zflag    Represents whether the z key has been pressed.
        @param pshare   Stores current position. Shared among tasks.
        @param dshare   Stores current delta. Shared among tasks.
    '''
    enc = encoder.Encoder(4,'PB6','PB7',pyb.Pin.AF2_TIM4) # Encoder object
    
    state = 1   # Initial state of FSM. Immediately begins data collection.
    
    start_time = ticks_us()     # Time stamp of when generator should complete
    next_time = ticks_add(start_time, period)   # next iteration
    
    while True:
        current_time = ticks_us() # represents current time
        
        if ticks_diff(current_time, next_time) >= 0: # adds period to current time to compute next time
            next_time = ticks_add(next_time, period)
            
            if state == 1: # Data Collection State; waits for inputs from user and updates position
                enc.update()
                pshare.write(enc.get_position())
                dshare.write(enc.get_delta())
                
                if zflag.read(): # Sets state to 2 if z key press detected
                    state = 2
                
            elif state == 2: # If state is 2, zero encoder and set back to state 1
                enc.zero()
                zflag.write(False)
                state = 1
                
        else:
            yield None