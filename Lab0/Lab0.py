# -*- coding: utf-8 -*-
"""
Created on Thu Jan  6 08:19:43 2022

@author: Scott Anderson
"""

def fibo(val):
    #Returns the number in the fibonacci squence at the requested index
    #dex is the integer index within the sequence
   
    
    if val == 1:
        return 1
    elif val == 2:
        return 1
    elif val <= 0:
        return 0
    else:
       # these are exceptions that are not properly handled by the following 
       #formula and are returned separately
        
        
        i = val #counting index starts at requested index value
        fibom1 = 1 #fibonacci number at index -1 starts at 1
        fibom2 = 1 #fibonacci number at index -2 starts at 1
        while(i-2):
            fiboNumber = fibom2 + fibom1 #output is sum of previous 2 in sequence
            fibom2 = fibom1 #new minus 2 value is the old minus 1 value
            fibom1 = fiboNumber #new minus 1 value is the old main value
            i = i-1 #increment the index value down
            
    #formula for any given number greater than 2 in the fibonacci sequence
    #it will continue adding numbers until the requested inex is reached

    return fiboNumber

    
if __name__ == '__main__':
    
    userInput = False
    #boolean for a valid uder input
    while userInput == False:
        dex = input('Enter the index for the desired number in the fibonacci sequence or type end:  ')
        #request and recieve input value
        if dex.strip().isdigit(): 
            intdex = int(dex)
            print('The Fibonacci number at ' + str(intdex) + ' is ' + str(fibo(intdex)) + '.')
            #runs the fibo() function if input is a valid positive integer
            
        elif dex == 'end': 
            userInput = True
            #input command to end the loop and program
            
        else:
            print('Requested index is not a positive integer. Try again or type end.')
            #error message when input is not a valid command or value
    

    